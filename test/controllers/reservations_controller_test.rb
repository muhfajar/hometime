# frozen_string_literal: true

require 'test_helper'

class ReservationsControllerTest < ActionDispatch::IntegrationTest
  test 'should handle existing reservation error' do
    # Prepare
    reservation_payload = {
      fixed_rate: 100,
      constant: 'Some constant value'
    }

    # Override the behavior of the insert_data method
    reservation_service = ReservationService.new

    def reservation_service.insert_data(_data)
      nil
    end

    # Act
    post reservations_url, params: reservation_payload.to_json, headers: { 'Content-Type': 'application/json' }

    # Assert
    assert_response :unprocessable_entity
    assert_equal 'application/json; charset=utf-8', response.content_type
    assert_equal({ error: I18n.t('error.failed') }.to_json, response.body)
  end
end
