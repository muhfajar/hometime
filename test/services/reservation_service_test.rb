# frozen_string_literal: true

require 'test_helper'

class ReservationServiceTest < ActiveSupport::TestCase
  def setup
    @service = ReservationService.new
  end

  def test_insert_data
    data = JSON.parse(load_fixture(:airbnb_payload))

    data_transform = ReservationDataTransform.call(RESERVATION_PAYLOAD_TYPE[:AIRBNB], data.deep_symbolize_keys!)

    @service.insert_data(data_transform.result)

    guest = Guest.find_by_email('test@muhfajar.id')
    reservation = Reservation.find_by_code('YYY12345678')

    assert_not_nil guest
    assert_not_nil reservation
    assert_equal 'Fajar', guest.last_name
    assert_equal %w[639123456789], guest.guest_details.pluck(:phone)
    assert_equal 'YYY12345678', reservation.code
  end

  def test_update_data
    data = JSON.parse(load_fixture(:bookingcom_payload))

    data_transform = ReservationDataTransform.call(RESERVATION_PAYLOAD_TYPE[:BOOKINGCOM], data.deep_symbolize_keys!)

    @service.insert_data(data_transform.result)

    guest = Guest.find_by_email('test@muhfajar.id')
    reservation = Reservation.find_by_code('XXX12345678')
    reservation.nights = 5

    assert_not_nil guest
    assert_not_nil reservation
    assert_not_equal data[:reservation][:nights], reservation.nights
    assert_equal 'Fajar', guest.last_name
    assert_equal %w[639123456789 639123456789], guest.guest_details.pluck(:phone)
    assert_equal 'XXX12345678', reservation.code
  end
end
