# frozen_string_literal: true

# == Schema Information
#
# Table name: guest_details
#
#  id         :integer          not null, primary key
#  phone      :string
#  guest_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
require 'test_helper'

class GuestDetailTest < ActiveSupport::TestCase
  def setup
    @detail1 = guest_details(:detail1)
    @detail2 = guest_details(:detail2)
    @detail3 = guest_details(:detail3)
  end

  test 'guest should be valid' do
    assert @detail1.valid?
    assert @detail2.valid?
    assert @detail3.valid?
  end

  test 'phone should be present at least one' do
    @detail1.phone = ''
    assert_not @detail1.valid?
    assert_equal ["Phone can't be blank"], @detail1.errors.full_messages
  end
end
