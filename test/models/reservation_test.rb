# frozen_string_literal: true

# == Schema Information
#
# Table name: reservations
#
#  id             :integer          not null, primary key
#  code           :string
#  start_date     :date
#  end_date       :date
#  nights         :integer
#  total_guests   :integer
#  adults         :integer
#  children       :integer
#  infants        :integer
#  status         :string
#  currency       :string
#  payout_price   :decimal(8, 2)
#  security_price :decimal(8, 2)
#  total_price    :decimal(8, 2)
#  guest_id       :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
require 'test_helper'

class ReservationTest < ActiveSupport::TestCase
  def setup
    @reservation1 = reservations(:reservation1)
    @reservation2 = reservations(:reservation2)
  end

  test 'should be valid with valid attributes' do
    assert @reservation1.valid?
    assert @reservation2.valid?
  end

  test 'should require presence of code' do
    @reservation1.code = nil
    assert_not @reservation1.valid?
    assert_includes @reservation1.errors[:code], "can't be blank"
  end

  test 'should require presence of start_date' do
    @reservation1.start_date = nil
    assert_not @reservation1.valid?
    assert_includes @reservation1.errors[:start_date], "can't be blank"
  end

  test 'should require presence of end_date' do
    @reservation1.end_date = nil
    assert_not @reservation1.valid?
    assert_includes @reservation1.errors[:end_date], "can't be blank"
  end

  test 'should require presence of status' do
    @reservation1.status = nil
    assert_not @reservation1.valid?
    assert_includes @reservation1.errors[:status], "can't be blank"
  end

  test 'reservation should have at least one night' do
    @reservation1.nights = 0
    assert_not @reservation1.valid?
    assert_includes @reservation1.errors[:nights], 'must be greater than or equal to 1'
  end

  test 'reservation should have at least one guest' do
    @reservation1.total_guests = 0
    assert_not @reservation1.valid?
    assert_includes @reservation1.errors[:total_guests], 'must be greater than or equal to 1'
  end

  test 'payout price should not free' do
    @reservation1.payout_price = 0
    assert_not @reservation1.valid?
    assert_includes @reservation1.errors[:payout_price], 'must be greater than 0'
  end

  test 'security price should not free' do
    @reservation1.security_price = 0
    assert_not @reservation1.valid?
    assert_includes @reservation1.errors[:security_price], 'must be greater than 0'
  end

  test 'total price should not free' do
    @reservation1.total_price = 0
    assert_not @reservation1.valid?
    assert_includes @reservation1.errors[:total_price], 'must be greater than 0'
  end

  # Add more validation tests for other attributes

  test 'should belong to a guest' do
    assert_respond_to @reservation1, :guest
    assert_instance_of Guest, @reservation1.guest
  end
end
