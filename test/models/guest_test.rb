# frozen_string_literal: true

# == Schema Information
#
# Table name: guests
#
#  id         :integer          not null, primary key
#  first_name :string
#  last_name  :string
#  email      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
require 'test_helper'

class GuestTest < ActiveSupport::TestCase
  def setup
    @guest1 = guests(:guest1)
    @guest2 = guests(:guest2)
  end

  test 'guest should be valid' do
    assert @guest1.valid?
    assert @guest2.valid?
  end

  test 'first name should be present' do
    @guest1.first_name = ''
    assert_not @guest1.valid?
    assert_equal ["First name can't be blank"], @guest1.errors.full_messages
  end

  test 'last name should be present' do
    @guest1.last_name = ''
    assert_not @guest1.valid?
    assert_equal ["Last name can't be blank"], @guest1.errors.full_messages
  end

  test 'email should be present' do
    @guest1.email = ''
    assert_not @guest1.valid?
    assert_equal ["Email can't be blank"], @guest1.errors.full_messages
  end

  test 'email should be unique' do
    duplicate_guest = @guest1.dup
    assert_not duplicate_guest.valid?
    assert_equal ['Email has already been taken'], duplicate_guest.errors.full_messages
  end

  # Add more validation tests for other attributes

  test 'guest should have at least one phone number' do
    guest = guests(:guest1)
    guest_detail = guest_details(:detail1)

    assert_equal 1, guest.guest_details.count
    assert_includes guest.guest_details, guest_detail
  end

  test 'phone can be added more than one' do
    guest = guests(:guest2)
    guest_detail1 = guest_details(:detail2)
    guest_detail2 = guest_details(:detail3)

    assert_equal 2, guest.guest_details.count
    assert_includes guest.guest_details, guest_detail1
    assert_includes guest.guest_details, guest_detail2
  end

  test 'should have many reservations' do
    guest = guests(:guest1)
    reservation1 = reservations(:reservation1)
    reservation2 = reservations(:reservation2)

    assert_equal 2, guest.reservations.count
    assert_includes guest.reservations, reservation1
    assert_includes guest.reservations, reservation2
  end
end
