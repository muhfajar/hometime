# frozen_string_literal: true

require 'test_helper'

class ReservationDataTransformTest < ActiveSupport::TestCase
  def setup
    @payload = {}
  end

  test '#call when payload_type is AIRBNB' do
    @payload = JSON.parse(load_fixture(:airbnb_payload))

    data = ReservationDataTransform.call(RESERVATION_PAYLOAD_TYPE[:AIRBNB], @payload.deep_symbolize_keys!)

    expected_result = {
      guest: {
        first_name: 'Muhamad',
        last_name: 'Fajar',
        email: 'test@muhfajar.id',
        phones: ['639123456789']
      },
      reservation: {
        code: 'YYY12345678',
        start_date: '2021-04-14',
        end_date: '2021-04-18',
        nights: 4,
        total_guests: 4,
        adults: 2,
        children: 2,
        infants: 0,
        status: 'accepted',
        currency: 'AUD',
        payout_price: '4200.00',
        security_price: '500',
        total_price: '4700.00'
      }
    }

    assert_equal expected_result, data.result
  end

  test '#call when payload_type is BOOKINGCOM' do
    @payload = JSON.parse(load_fixture(:bookingcom_payload))

    data = ReservationDataTransform.call(RESERVATION_PAYLOAD_TYPE[:BOOKINGCOM], @payload.deep_symbolize_keys!)

    expected_result = {
      guest: {
        first_name: 'Muhamad',
        last_name: 'Fajar',
        email: 'test@muhfajar.id',
        phones: %w[639123456789 639123456789]
      },
      reservation: {
        code: 'XXX12345678',
        start_date: '2021-03-12',
        end_date: '2021-03-16',
        nights: 4,
        total_guests: 4,
        adults: 2,
        children: 2,
        infants: 0,
        status: 'accepted',
        currency: 'AUD',
        payout_price: '3800.00',
        security_price: '500.00',
        total_price: '4300.00'
      }
    }

    assert_equal expected_result, data.result
  end

  test '#call when payload_type is unknown' do
    expected_result = {}
    data = ReservationDataTransform.call(nil, @payload)
    assert_equal expected_result, data.result
  end
end
