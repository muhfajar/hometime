# frozen_string_literal: true

require 'test_helper'

class ReservationPayloadSwitcherTest < ActiveSupport::TestCase
  def setup
    @payload = {}
  end

  test '#call when payload_type is AIRBNB' do
    @payload = JSON.parse(load_fixture(:airbnb_payload))

    data = ReservationPayloadSwitcher.call(@payload)
    expected_result = RESERVATION_PAYLOAD_TYPE[:AIRBNB]

    assert_equal expected_result, data.result
  end

  test '#call when payload_type is BOOKINGCOM' do
    @payload = JSON.parse(load_fixture(:bookingcom_payload))

    data = ReservationPayloadSwitcher.call(@payload)
    expected_result = RESERVATION_PAYLOAD_TYPE[:BOOKINGCOM]

    assert_equal expected_result, data.result
  end

  test '#call when payload_type is unknown' do
    expected_result = RESERVATION_PAYLOAD_TYPE[:UNKNOWN]
    data = ReservationPayloadSwitcher.call(@payload)
    assert_equal expected_result, data.result
  end
end
