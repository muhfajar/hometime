# frozen_string_literal: true

# == Schema Information
#
# Table name: reservations
#
#  id             :integer          not null, primary key
#  code           :string
#  start_date     :date
#  end_date       :date
#  nights         :integer
#  total_guests   :integer
#  adults         :integer
#  children       :integer
#  infants        :integer
#  status         :string
#  currency       :string
#  payout_price   :decimal(8, 2)
#  security_price :decimal(8, 2)
#  total_price    :decimal(8, 2)
#  guest_id       :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
class CreateReservations < ActiveRecord::Migration[7.0]
  def change
    create_table :reservations do |t|
      t.string :code
      t.date :start_date
      t.date :end_date
      t.integer :nights
      t.integer :total_guests
      t.integer :adults
      t.integer :children
      t.integer :infants
      t.string :status
      t.string :currency
      t.decimal :payout_price, precision: 8, scale: 2
      t.decimal :security_price, precision: 8, scale: 2
      t.decimal :total_price, precision: 8, scale: 2
      t.belongs_to :guest, foreign_keys: true

      t.timestamps
    end

    add_index :reservations, :code, unique: true
  end
end
