# frozen_string_literal: true

# == Schema Information
#
# Table name: guest_details
#
#  id         :integer          not null, primary key
#  phone      :string
#  guest_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class CreateGuestDetails < ActiveRecord::Migration[7.0]
  def change
    create_table :guest_details do |t|
      t.string :phone
      t.belongs_to :guest, foreign_keys: true

      t.timestamps
    end
  end
end
