# frozen_string_literal: true

# ReservationPayloadSwitcher - app/commands/reservation_payload_switcher.rb
#
# This command is used for switching payload, to determine
# witch one is payload from Airbnb or booking.com.
#
class ReservationPayloadSwitcher
  prepend SimpleCommand

  def initialize(payload)
    @payload = payload
  end

  def call
    base_path = Rails.root.join('app/validators/schemas/')

    # load available json schema
    airbnb_path = Rails.root.join(base_path, 'airbnb_payload.json')
    airbnb_schema = JSON.parse(File.read(airbnb_path))

    bookingcom_path = Rails.root.join(base_path, 'bookingcom_payload.json')
    bookingcom_schema = JSON.parse(File.read(bookingcom_path))

    if JSON::Validator.validate(airbnb_schema, @payload)
      RESERVATION_PAYLOAD_TYPE[:AIRBNB]
    elsif JSON::Validator.validate(bookingcom_schema, @payload)
      RESERVATION_PAYLOAD_TYPE[:BOOKINGCOM]
    else
      RESERVATION_PAYLOAD_TYPE[:UNKNOWN]
    end
  end
end
