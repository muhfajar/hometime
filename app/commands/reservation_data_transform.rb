# frozen_string_literal: true

# ReservationDataTransform - app/commands/reservation_data_transform.rb
#
# This command is used for transforming data from payload
# to make it fit with database schema.
#
class ReservationDataTransform
  prepend SimpleCommand

  def initialize(payload_type, payload)
    @payload_type = payload_type
    @payload = payload
  end

  def call
    case @payload_type
    when RESERVATION_PAYLOAD_TYPE[:AIRBNB]
      airbnb_reservation_data(@payload)
    when RESERVATION_PAYLOAD_TYPE[:BOOKINGCOM]
      bookingcom_reservation_data(@payload)
    else
      {}
    end
  end

  private

  def airbnb_reservation_data(payload)
    {
      guest: {
        first_name: payload[:guest][:first_name],
        last_name: payload[:guest][:last_name],
        email: payload[:guest][:email],
        phones: [
          payload[:guest][:phone]
        ]
      },
      reservation: {
        code: payload[:reservation_code],
        start_date: payload[:start_date],
        end_date: payload[:end_date],
        nights: payload[:nights],
        total_guests: payload[:guests],
        adults: payload[:adults],
        children: payload[:children],
        infants: payload[:infants],
        status: payload[:status],
        currency: payload[:currency],
        payout_price: payload[:payout_price],
        security_price: payload[:security_price],
        total_price: payload[:total_price]
      }
    }
  end

  def bookingcom_reservation_data(payload)
    {
      guest: {
        first_name: payload[:reservation][:guest_first_name],
        last_name: payload[:reservation][:guest_last_name],
        email: payload[:reservation][:guest_email],
        phones: payload[:reservation][:guest_phone_numbers]
      },
      reservation: {
        code: payload[:reservation][:code],
        start_date: payload[:reservation][:start_date],
        end_date: payload[:reservation][:end_date],
        nights: payload[:reservation][:nights],
        total_guests: payload[:reservation][:number_of_guests],
        adults: payload[:reservation][:guest_details][:number_of_adults],
        children: payload[:reservation][:guest_details][:number_of_children],
        infants: payload[:reservation][:guest_details][:number_of_infants],
        status: payload[:reservation][:status_type],
        currency: payload[:reservation][:host_currency],
        payout_price: payload[:reservation][:expected_payout_amount],
        security_price: payload[:reservation][:listing_security_price_accurate],
        total_price: payload[:reservation][:total_paid_amount_accurate]
      }
    }
  end
end
