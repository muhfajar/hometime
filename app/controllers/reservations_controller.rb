# frozen_string_literal: true

# ReservationsController - app/controllers/reservations_controller.rb
#
# This controller handles the register of reservation from Airbnb and booking.com
#
# All actions in this controller is not require authentication.
#
# The API responses are in JSON format, and the expected request and response structures are
# documented in the corresponding action comments.
#
class ReservationsController < ApplicationController
  # POST /reservations
  # Creates a reservation data with detail of guest
  # Params:
  #   - have two type of reservation payload:
  #     1. Airbnb → app/validators/schemas/airbnb_payload.json
  #     2. Booking.com → app/validators/schemas/bookingcom_payload.json
  #
  def create
    payload_type = ReservationPayloadSwitcher.call(request.body.read)

    if payload_type.result == RESERVATION_PAYLOAD_TYPE[:UNKNOWN]
      render json: { error: I18n.t('error.failed') }, status: :unprocessable_entity
    else
      data = ReservationDataTransform.call(payload_type.result, params)
      reservation = ReservationService.new
      result = reservation.insert_data(data.result)

      return render json: { error: I18n.t('error.exist') }, status: :unprocessable_entity if result.nil?

      render json: { success: true }, status: :ok
    end
  end

  private

  def airbnb_params(param)
    param.permit(:fixed_rate, :constant)
  end

  def bookingcom_params(param)
    param.permit(:fixed_rate, :constant)
  end
end
