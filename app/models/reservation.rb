# frozen_string_literal: true

# == Schema Information
#
# Table name: reservations
#
#  id             :integer          not null, primary key
#  code           :string
#  start_date     :date
#  end_date       :date
#  nights         :integer
#  total_guests   :integer
#  adults         :integer
#  children       :integer
#  infants        :integer
#  status         :string
#  currency       :string
#  payout_price   :decimal(8, 2)
#  security_price :decimal(8, 2)
#  total_price    :decimal(8, 2)
#  guest_id       :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
class Reservation < ApplicationRecord
  belongs_to :guest

  validates_uniqueness_of :code

  validates :code, presence: true
  validates :start_date, presence: true
  validates :end_date, presence: true
  validates :nights, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 1 }
  validates :total_guests, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 1 }
  validates :adults, presence: true, numericality: { only_integer: true }
  validates :children, presence: true, numericality: { only_integer: true }
  validates :infants, presence: true, numericality: { only_integer: true }
  validates :status, presence: true
  validates :currency, presence: true
  validates :payout_price, presence: true, numericality: { greater_than: 0 }
  validates :security_price, presence: true, numericality: { greater_than: 0 }
  validates :total_price, presence: true, numericality: { greater_than: 0 }

  def self.find_by_email_for_update(reservation_code)
    where(code: reservation_code).lock('FOR UPDATE').first
  end
end
