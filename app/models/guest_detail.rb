# frozen_string_literal: true

# == Schema Information
#
# Table name: guest_details
#
#  id         :integer          not null, primary key
#  phone      :string
#  guest_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class GuestDetail < ApplicationRecord
  belongs_to :guest

  validates :phone, presence: true
end
