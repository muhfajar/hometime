# frozen_string_literal: true

# == Schema Information
#
# Table name: guests
#
#  id         :integer          not null, primary key
#  first_name :string
#  last_name  :string
#  email      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Guest < ApplicationRecord
  has_many :reservations
  has_many :guest_details

  validates_uniqueness_of :email

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :email, presence: true, uniqueness: true

  def self.find_by_email_for_update(user_email)
    where(email: user_email).lock('FOR UPDATE').first
  end
end
