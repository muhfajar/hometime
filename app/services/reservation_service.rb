# frozen_string_literal: true

# ReservationService - app/service/reservations_service.rb
#
# This service handles for CRUD operation for reservation.
#
class ReservationService
  def insert_data(data)
    Guest.transaction do
      # Guest cannot be update
      # 5. API should be able accept changes to the reservation.
      #    e.g., change in status, check-in/out dates, number of guests, etc...
      guest_data = data[:guest].except(:phones)
      @guest = Guest.new(guest_data)
      @guest.save!
    rescue ActiveRecord::RecordInvalid
      @guest = Guest.find_by_email(data[:guest][:email])
    end

    if data[:guest][:phones].presence
      # since phone data is not consistent between Airbnb and booking.com, to make data always up to date
      # we need to delete the old one before we update with updated phone data from request.
      #
      # another approaching for this kind of data, we can merge into one string with delimiter.
      # e.g. 123|456
      GuestDetail.transaction do
        GuestDetail.where(guest_id: @guest.id).delete_all if GuestDetail.exists?(guest_id: @guest.id)

        data[:guest][:phones].each do |phone_number|
          GuestDetail.create(guest_id: @guest.id, phone: phone_number)
        end
      end
    end

    Reservation.transaction do
      reservation_data = data[:reservation].merge(guest_id: @guest.id)
      Reservation.upsert(reservation_data, unique_by: :code)
    end
  end
end
