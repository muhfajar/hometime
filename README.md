# Project Setup and Execution Guide

This README file provides instructions on how to set up and run a Rails API.

### Prerequisites
Before setting up the Rails API, ensure that the following dependencies are installed on your system:

1. **Ruby**: Ensure you have Ruby installed. Recommended using `rbenv` →  [https://github.com/rbenv/rbenv](https://github.com/rbenv/rbenv#installation)
```
$ ruby --version
ruby 3.2.2 (2023-03-30 revision e51014f9c0) [arm64-darwin22]
```

2. **Rails**: Install the Rails gem by running `gem install rails` in your terminal.
```
$ rails --version
Rails 7.0.4
```

3. **Database**: You will also need an installation of the SQLite3 database. For instructionshow to install in your machine, you can find at the https://www.sqlite.org/
```
$ sqlite3 --version
3.39.5 2022-10-14 20:58:05 554764a6e721fab307c63a4f98cd958c8428a5d9d8edfde951858d6fd02daapl
```
---

#### Step 1: Clone this repository
Begin by cloning the repository containing your Rails API project. Use the following command in your terminal:
```
git clone git@gitlab.com:muhfajar/hometime.git
```

#### Step 2: Install Dependencies
Navigate to the project directory and install the required dependencies using Bundler. Run the following command:
```
cd hometime
bundle install
```
This command will install all the gems specified in the project's Gemfile.

#### Step 3: Configure the Database
Configure your Rails application to connect to the database. Open the `config/database.yml` file and update the settings
according to your database configuration.

#### Step 4: Create and Migrate the Database
Create the database by running the following command:
```
rails db:migrate
```
Next, run the migrations to set up the required database schema:

#### Step 5: Run the Rails Server
You are now ready to start the Rails server. Execute the following command:
```
rails server
```
By default, the server will start on http://localhost:3000. You can access the API using this URL.

#### Step 6: Test the API
To verify that your Rails API is running correctly, you can send requests to the server using a tool like cURL, Postman,
or your preferred API testing tool. Here sample request with curl:
```
curl -X "POST" "http://localhost:3000/reservations" \
     -H 'Content-Type: application/json; charset=utf-8' \
     -d $'{
  "reservation": {
    "start_date": "2021-03-12",
    "host_currency": "AUD",
    "guest_details": {
      "localized_description": "4 guests",
      "number_of_infants": 0,
      "number_of_children": 2,
      "number_of_adults": 2
    },
    "guest_phone_numbers": [
      "639123456789",
      "639123456789"
    ],
    "end_date": "2021-03-16",
    "number_of_guests": 5,
    "nights": 4,
    "listing_security_price_accurate": "500.00",
    "code": "XXX12345678",
    "guest_first_name": "Wayne",
    "guest_email": "wayne_woodbridge@bnb.com",
    "status_type": "accepted",
    "total_paid_amount_accurate": "4300.00",
    "expected_payout_amount": "3800.00",
    "guest_last_name": "Woodbridge"
  }
}'

```
