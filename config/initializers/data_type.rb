# frozen_string_literal: true

RESERVATION_PAYLOAD_TYPE = {
  AIRBNB: 'airbnb',
  BOOKINGCOM: 'bookingcom',
  UNKNOWN: 'unknown'
}.freeze
