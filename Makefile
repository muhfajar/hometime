annotate:
	bundle exec annotate --models

migrate:
	bundle exec rails db:migrate

run:
	bundle exec rails server

test:
	bundle exec rails test